# 👋 Hi, I’m Soroosh Shalileh 


## About Me:
  - 🌱 I’m a Machine Learning Researcher at [LAMBDA Lab](https://cs.hse.ru/en/lambda/), and also a [Ph.D. candidate](https://www.hse.ru/en/org/persons/316426865) at the [Faculty of Computer Science](https://cs.hse.ru/en/), [School of Data Analysis and Artificial Intelligent](https://cs.hse.ru/en/ai/), Moscow, Russia.


- 📫 You can reach me using: sr.shalileh@gmail.com 

## Recent Publications

- Shalileh, S. and Mirkin, B., 2021. Least-squares community extraction in feature-rich networks using similarity data. Plos one, 16(7), p.e0254377. https://doi.org/10.1371/journal.pone.0254377
- Shalileh, S., Mirkin, B. Summable and nonsummable data-driven models for community detection in feature-rich networks. Soc. Netw. Anal. Min. 11, 67 (2021). https://doi.org/10.1007/s13278-021-00774-8
- Shalileh, S. and Mirkin, B., 2020, November. A One-by-One Method for Community Detection in Attributed Networks. In International Conference on Intelligent Data Engineering and Automated Learning (pp. 413-422). Springer, Cham. https://doi.org/10.1007/978-3-030-62365-4_39 
- Shalileh, S. and Mirkin, B., 2020, December. A Method for Community Detection in Networks with Mixed Scale Features at Its Nodes. In International Conference on Complex Networks and Their Applications (pp. 3-14). Springer, Cham. https://doi.org/10.1007/978-3-030-65347-7_1.


For more check my [Google Scholar](), [ORCID](https://orcid.org/0000-0001-6226-4990) and [Scopus](https://www.scopus.com/authid/detail.uri?partnerID=HzOxMe3b&authorId=57202057084&origin=inward).




<!---
Sorooshi/Sorooshi is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
